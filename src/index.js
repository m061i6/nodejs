//------------require-----------//
const express = require('express');
const session = require('express-session');
const multer = require('multer');
const fs = require('fs');
const cors = require('cors');
const axios = require('axios');
const cheerio = require('cheerio');

const app = express();

const upload = multer({
    dest: 'tmp_uploads'
});

//------------set-----------//
app.set('view engine', 'ejs');

//------------use-----------//
app.use(cors());
//Top-level Middleware
// 解析 post urlencoded 的 middleware
app.use(express.urlencoded({
    extended: false
}));
// 解析 json 的 middleware
app.use(express.json());

app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'm061i6',
    cookie: {
        maxAge: 1200000
    }
}));


app.use((req, res, next)=>{
    res.locals.pageName = '';
    res.locals.isAdmin = false;
    req.query.from_middleware = 'hello';
    if(req.session.admins && req.session.admins.account){
        res.locals.admins = req.session.admins;
        res.locals.isAdmin = true;
    }
    next();
});

app.use('/address-book', require(__dirname + '/address-book.js'));



//------------get-----------//
const db = require(__dirname + '/__connect_db.js');


app.get('/', (req, res) => {
    res.render('home', {
        name: 'hello world'
    });
});

app.get('/', (req, res) => {
    res.render('home', {
        name: 'hello world'
    });
});
app.get('/try-qs', (req, res) => {
    res.json(req.query);
});
app.get('/just-pending', (req, res) => {
    res.json(req.query);
});



//middleware
// const urlencodedParser = express.urlencoded({extended: false});
app.get('/try-post-form', (req, res) => {
    res.render('try-post-form');
});
app.post('/try-post-form', (req, res) => {
    res.render('try-post-form', req.body);
});
app.post('/try-post-json', (req, res) => {
    res.json(req.body);
});
app.get('/try-plus', (req, res) => {
    const output = {
        postData: req.body
    };
    let a = req.body.a * 1 || 0;
    let b = req.body.b * 1 || 0;
    output.result = a + b;

    res.json(output);
});
app.post('/try-plus', (req, res) => {
    let a = req.body.a * 1 || 0;
    let b = req.body.b * 1 || 0;
    res.send(a + b + '');
});
app.post('/try-plus2', (req, res) => {
    const output = {
        postData: req.body
    };
    let a = req.body.a * 1 || 0;
    let b = req.body.b * 1 || 0;
    output.result = a + b;

    res.json(output);
});
app.post('/try-upload', upload.single('avatar'), (req, res) => {
    let ext = '';
    switch (req.file.mimetype) {
        case 'image/png':
            ext = '.png';
            break;
        case 'image/jpeg':
            ext = '.jpg';
            break;
    }
    if (ext) {
        let filename = (new Date().getTime()) + ext;
        fs.rename(
            req.file.path,
            './public/img/' + filename,
            error => {
                res.json({
                    success: true,
                    img: '/img/' + filename,
                    body: req.body
                });
            }
        )
    } else {
        fs.unlink(res.file.path, error => {
            res.json({
                success: false,
                info: 'wrong file type'
            });
        });
    }
});

app.get('/json', (req, res) => {
    const data = require(__dirname + '/../data/sales.json');
    //res.send(data);
    res.render('sales', {
        sales: data
    });
});

app.get(/^\/mobile\/09\d{2}-?\d{3}-?\d{3}$/i, (req, res) => {
    let u = req.url.slice(8).split('?')[0];
    let m = u.split('-').join('');
    res.json({
        'url': req.url,
        'mobile': m
    });
});

const admin1 = require(__dirname + '/admins/admin1.js');
admin1(app);

const admin2 = require(__dirname + '/admins/admin2.js');
// app.use(admin2);
app.use('/my-path', admin2);

const admin3 = require(__dirname + '/admins/admin3.js');
app.use('/member', admin3);

// app.get('/my-params1/:action?/:id?', (req, res) => {
//     res.json(req.params);
// });

app.get('/try-session', (req, res) => {
    req.session.my_var = req.session.my_var || 0
    req.session.my_var++;
    res.json({
        my_var : req.session.my_var,
        session : req.session
    })
});
app.get('/try-db', (req, res) => {
   let sql = "SELECT * FROM `address_book` ORDER BY `name`";
   db.query(sql, (error , result , fields)=>{
        console.log(fields);
        res.json(result);
   });
});

app.get('/try-axios', (req, res)=>{
    axios.get('https://tw.yahoo.com/')
        .then(response=>{
            let $ = cheerio.load(response.data);
            let str = '';
            $('.MainStoryImage').each(function(i,el){
                let src = $(this).attr('src');
                str += `<img src="${src}"><br>`;
            })

            res.send( str );
        })
});

app.use(express.static('public')); //設定server根目錄為public

app.use((req, res) => {
    res.type('text/html');
    res.status(404);
    res.send(`
        <h2>Page not Found !!!</h2>
    `);
});



app.listen(3000, () => {
    console.log('server started!');
});