const express = require('express');
const router = express.Router();
router.route('/edit/:id')
    .all((req, res, next) => {
        res.locals.membetdata = {
            name: 'jack',
            id: 'm061i6'
        }
        next();
    })
    .get((req, res) => {
        const obj = {
            baseUrl : req.baseUrl,
            url: req.url,
            data: res.locals.membetdata
        };
        res.send('get edit:'+ JSON.stringify(obj));
    })
    .post((req, res) => {
        res.send('post edit:'+ JSON.stringify(res.locals.membetdata));
    })

module.exports = router;