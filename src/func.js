let func1 = a => a * a;
let func2 = () => {
    let t = 0;
    for (let i = 1; i <= 10; i++) {
        t += i;
    }
    return t;
};

console.log(func1(5));
console.log(func2());