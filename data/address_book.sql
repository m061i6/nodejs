-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2020-04-21 08:50:31
-- 伺服器版本： 10.4.11-MariaDB
-- PHP 版本： 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


--
-- 資料庫： `aien07`
--

-- --------------------------------------------------------

--
-- 資料表結構 `address_book`
--

CREATE TABLE `address_book` (
  `sid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `address_book`
--

INSERT INTO `address_book` (`sid`, `name`, `email`, `mobile`, `birthday`, `address`, `created_at`) VALUES
(1, '李小明', 'mmm@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(2, '李小明1', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(3, '李小明2', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(4, '李小明3', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(5, '李小明4', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(6, '李小明5', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(7, '李小明6', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(8, '李小明7', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(9, '李小明8', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(10, '李小明9', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(11, '李小明10', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47'),
(12, '李小明10', 'mmm1@ggg.com', '0918333666', '1990-03-02', '台北市', '2020-04-21 14:44:47');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`sid`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `address_book`
--
ALTER TABLE `address_book`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

